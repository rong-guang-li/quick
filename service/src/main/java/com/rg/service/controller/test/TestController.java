package com.rg.service.controller.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2024-03-29  16:30
 */

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("hello")
    public String test(){
        return "hello";
    }
}
